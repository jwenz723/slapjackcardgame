﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Drawing;
using System.Media;
using System.Reflection;
using System.ComponentModel;

namespace _SlapJack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Properties

        /// <summary>
        /// An object to represent the deck that is being used
        /// </summary>
        Deck deck;

        /// <summary>
        /// An object to represent the player
        /// </summary>
        Deck player1;

        /// <summary>
        /// An object to represent the dealer
        /// </summary>
        Deck player2;

        Stack<Card> pile = new Stack<Card>();

        private int playerTurn = 1;

        /// <summary>
        /// An image object used for loading all the card images
        /// </summary>
        BitmapImage[,] cardImages = new BitmapImage[3, 7];

        /// <summary>
        /// If this is set to true then the game is over
        /// </summary>
        bool gameOver = false;

        /// <summary>
        /// Indicates that the start button has been pressed
        /// </summary>
        bool gameStarted = false;

        /// <summary>
        /// The splashscreen storyboard
        /// </summary>
        Storyboard sb1;

        #endregion

        #region Starting Program Code
        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            try
            {
                InitializeComponent();

                // get the splash screen storyboards
                sb1 = this.FindResource("splashScreen") as Storyboard;
                //sb2 = this.FindResource("splashScreen2") as Storyboard;

                // start the splash screen storyboards
                sb1.Begin();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// This function is called when the default close button 'X' is clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            // kill the storyboard animations that occur on the splash screen
            sb1.Stop();
        }
       
        #endregion

        #region Other Methods

        /// <summary>
        /// Keyboard listener
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            try
            {
                // if the start button has been pressed and the game is not over
                if (gameStarted && !gameOver)
                {
                    try
                    {
                        if (e.Key == Key.Z)
                        {
                            System.Console.WriteLine("1 slap pressed");
                            GameLogicSlap(1);
                        }

                        if (e.Key == Key.X)
                        {
                            System.Console.WriteLine("1 flip pressed");
                            revealCard(1);
                        }

                        if (e.Key == Key.OemQuestion)
                        {
                            System.Console.WriteLine("2 slap pressed");
                            GameLogicSlap(2);
                        }

                        if (e.Key == Key.OemPeriod)
                        {
                            System.Console.WriteLine("2 flip pressed");
                            revealCard(2);
                        }

                        // update the card counters
                        updateCardCounts();
                    }
                    catch(Exception es){
                        System.Console.WriteLine("tried to do something crazy");
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        
        /// <summary>
        /// Function that handles all the logic of when one of the players "Slaps"
        /// </summary>
        /// <param name="playerNum">The player that slapped</param>
        private void GameLogicSlap(int playerNum)
        {
            if (playerNum == 1)
            {
                if (pile.Peek().faceValue == 11)
                {
                    player1.add(pile);
                    pile.Clear();

                    // animate a card onto the player1 pile
                    Storyboard cardAnimation = this.FindResource("passCardsToP1") as Storyboard;
                    cardAnimation.Begin();
                }
                else {
                    player2.add(pile);
                    pile.Clear();

                    // animate a card onto the player1 pile
                    Storyboard cardAnimation = this.FindResource("passCardsToP2") as Storyboard;
                    cardAnimation.Begin();
                }
            }
            else
            {
                if (pile.Peek().faceValue == 11)
                {
                    player2.add(pile);
                    pile.Clear();

                    // animate a card onto the player1 pile
                    Storyboard cardAnimation = this.FindResource("passCardsToP2") as Storyboard;
                    cardAnimation.Begin();
                }
                else
                {
                    player1.add(pile);
                    pile.Clear();

                    // animate a card onto the player1 pile
                    Storyboard cardAnimation = this.FindResource("passCardsToP1") as Storyboard;
                    cardAnimation.Begin();
                }

            }

            checkWin();
        
        }

        /// <summary>
        /// Checks if one of the players won the game (meaning one of the players is out of cards)
        /// or if both players ran out of cards.
        /// </summary>
        private void checkWin() {
            if (player1.deckSize() == 52) {
                weHaveAWinner(1);
            }
            else if (player2.deckSize() == 52 )
            {
                weHaveAWinner(2);
            }
            else if (player1.deckSize() == 0 && player2.deckSize() == 0)
            {
                weHaveAWinner(3);
            }

            if (player1.deckSize() == 0)
            {
                weHaveAWinner(2);
            }

            if (player2.deckSize() == 0)
            {
                weHaveAWinner(1);
            }
        }

        /// <summary>
        /// When the start game button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartGame_Click(object sender, RoutedEventArgs e)
        {
            try {
                gameStarted = true;

                // kill the storyboard animations that occur on the splash screen
                sb1.Stop();

                // deal the cards to the 2 players
                dealTheCards();

                // set the background of the card pile image to display a prompt to player 1 to press 'X'
                player1TopCard.Source = new BitmapImage(new Uri("Assets/player1message.png", UriKind.RelativeOrAbsolute));

                // Play the card dealing sound
                playDealSound();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// A function to update the number that is displayed on the screen
        /// to represent how many cards are in each of the player's decks.
        /// </summary>
        private void updateCardCounts()
        {
            try
            {
                // update the card counters
                lblCardsInHandP1.Content = player1.deckSize().ToString();
                lblCardsInHandP2.Content = player2.deckSize().ToString();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function shuffles the deck then deals the cards to the 2 player decks
        /// </summary>
        public void dealTheCards()
        {
            try
            {
                // initialize the deck
                deck = new Deck();

                // some temp variables to receive the player cards
                Card[] player1Cards;
                Card[] player2Cards;

                // get the cards for each player
                deck.split(out player1Cards, out player2Cards);

                // create the players
                player1 = new Deck(player1Cards);
                player2 = new Deck(player2Cards);

                // update the card counters
                updateCardCounts();

                // show the player 1 turn indicator
                tbPlayer1TurnIndicator.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function will reveal the card that is on the top of each player's deck if there is a card
        /// </summary>
        public void revealCard(int playerNum)
        {
            

            try
            {
                


                if (playerTurn == 1 && playerNum == 1 && player1.deckSize() == 0)
                {
                    playerTurn = 2;
                    tbPlayer1TurnIndicator.Visibility = Visibility.Hidden;
                    tbPlayer2TurnIndicator.Visibility = Visibility.Visible;
                }

                if (playerTurn == 2 && playerNum == 2 && player1.deckSize() == 0)
                {
                    playerTurn = 1;
                    tbPlayer2TurnIndicator.Visibility = Visibility.Hidden;
                    tbPlayer1TurnIndicator.Visibility = Visibility.Visible;
                }
       
                if (playerTurn == 1 && playerNum == 1 && player1.deckSize() > 0)
                {
                    // hide the player 1 turn indicator and show the player 2 indicator
                    tbPlayer1TurnIndicator.Visibility = Visibility.Hidden;
                    tbPlayer2TurnIndicator.Visibility = Visibility.Visible;

                    // animate a card onto the card pile
                    Storyboard cardAnimation = this.FindResource("player1Flip") as Storyboard;
                    cardAnimation.Begin();

                    playerTurn = 2;

                    Card temp = player1.deal();

                    // push the card onto the card pile stack
                    pile.Push(temp);

                    if (temp.imagePath != "nocard")
                    {
                        // display the card that is on the top of each player's deck
                        player1TopCard.Source = new BitmapImage(new Uri(temp.imagePath, UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show("nocard card");
                    }
                }
                else if (playerTurn == 2 && playerNum == 2 && player2.deckSize() > 0) 
                {
                    // hide the player 2 turn indicator and show the player 1 indicator
                    tbPlayer2TurnIndicator.Visibility = Visibility.Hidden;
                    tbPlayer1TurnIndicator.Visibility = Visibility.Visible;

                    // animate a card onto the card pile
                    Storyboard cardAnimation = this.FindResource("player2Flip") as Storyboard;
                    cardAnimation.Begin();

                    playerTurn = 1;

                    Card temp = player2.deal();

                    // push the card onto the card pile stack
                    pile.Push(temp);

                    if (temp.imagePath != "nocard")
                    {
                        // display the card that is on the top of each player's deck
                        player1TopCard.Source = new BitmapImage(new Uri(temp.imagePath, UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show("nocard card");
                    }
                }

                if (player1.deckSize() == 0 || player2.deckSize() == 0)
                {
                    checkWin();
                }
                

            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function performs actions that need to occur when we have a winning player
        /// </summary>
        private void weHaveAWinner(int winner)
        {
            try
            {
                if (!gameOver)
                {
                    gameOver = true;

                    // play a tada sound effect
                    using (SoundPlayer player = new SoundPlayer(@"../../Assets/tada.wav"))
                    {
                        player.Play();
                    }

                    // animate the cards off of the screen and the winning message on to the screen
                    Storyboard cardAnimation = this.FindResource("endOfGame") as Storyboard;
                    cardAnimation.Begin();

                    // Display a Message
                    if (winner != 3)
                    {
                        lblInstructions.Content = "Good Job Player " + winner + "!";
                    }
                    else
                    {
                        // show a message that tells the users to slap
                        //lblCardFound.Visibility = Visibility.Visible;
                        lblInstructions.Content = "You Guys Never Slap! Goodbye.";
                    }
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This function will play the card dealing sound
        /// </summary>
        private void playDealSound()
        {
            try
            {
                // Create new SoundPlayer in the using statement.
                using (SoundPlayer player = new SoundPlayer(@"../../Assets/cards.wav"))
                {
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

        #region Error handler method
        /// <summary>
        /// Handles an error
        /// </summary>
        /// <param name="sClass">The class in which the error occurred</param>
        /// <param name="sMethod">The method in which the error occurred</param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            } //end try

            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
        #endregion
    }
}
