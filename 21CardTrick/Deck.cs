﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _SlapJack
{
    class Deck
    {
        #region Properties
        /// <summary>
        /// Contains the 52 cards.  I used a stack because when playing black jack you
        /// should only have access to the card on the top of the deck. This is how stacks are.
        /// </summary>
        private Queue<Card> cards;

        /// <summary>
        /// Contains all the cards in default order (Hearts, Diamonds, Spades, then Clubs).
        /// This data structure is used for the shuffle function.  This structure should not
        /// be used as the actual usable "Deck"
        /// </summary>
        private Card[] unusableCards;
        #endregion

        #region Functions
        /// <summary>
        /// Default Constructor. Creates a deck that contains 52 cards in order.
        /// </summary>
        public Deck()
        {
            try
            {
                // Instantiate the deck
                cards = new Queue<Card>();

                // Instantiate the unusableCards (The default unshuffled deck)
                unusableCards = new Card[52];

                // load in the cards into unusableCards (The default unshuffled deck)
                for (int i = 0; i < 13; i++)
                {
                    // create new cards
                    Card newHeart = new Card(i + 1, Suits.Hearts);
                    Card newDiamond = new Card(i + 1, Suits.Diamonds);
                    Card newSpade = new Card(i + 1, Suits.Spades);
                    Card newClub = new Card(i + 1, Suits.Clubs);

                    // Add the cards to the unusable deck (default unshuffled deck)
                    unusableCards[i] = newHeart;
                    unusableCards[i + 13] = newDiamond;
                    unusableCards[i + 26] = newSpade;
                    unusableCards[i + 39] = newClub;
                }

                // Shuffle the deck 4 times
                shuffleDeck(4);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        //overloaded Deck constructor
        public Deck(Card[] cardArray)
        {
            cards = new Queue<Card>();

            foreach (Card card in cardArray)
            {
                cards.Enqueue(card);
            }
        }

        /// <summary>
        /// This function returns the card that is on the top of the deck
        /// </summary>
        /// <returns></returns>

        /// <summary>
        /// Shuffles the cards and places them into cards (the stack representing the shuffled deck).
        /// </summary>
        /// <param name="numTimesToShuffle">How many times do you want the deck to be shuffled? (Default is 1)</param>
        public void shuffleDeck(int numTimesToShuffle = 1)
        {
            try
            {
                // define a randomizer
                Random rand = new Random();

                // Copy the default deck
                Card[] newDeck = unusableCards;

                // Loop through however many times the parameter is set to
                for (int i = 0; i < numTimesToShuffle; i++)
                {
                    // loop through for each object contained in newDeck (52 times)
                    for (int n = newDeck.Length - 1; n > 0; --n)
                    {
                        // Swap the current card with a random card in the deck
                        int k = rand.Next(n + 1);
                        Card temp = newDeck[n];
                        newDeck[n] = newDeck[k];
                        newDeck[k] = temp;
                    }
                }

                // Clear out the deck
                cards.Clear();

                // push the newly shuffled deck into cards (stack)
                foreach (Card pushCard in newDeck)
                {
                    add(pushCard);
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        //Adds one card to the Deck
        public void add(Card c)
        {
            cards.Enqueue(c);
        }

        //Adds a stack of cards to the Deck   
        public void add(Stack<Card> sc)
        {
            foreach (Card card in sc)
            {
                add(card);
            }
        }

        //pulls the top card off the deck and returns it
        public Card deal()
        {
            if (cards.Count > 0)
            {
                return cards.Dequeue();
            }
            else
            {
                Card temp = new Card();

                // setting the imagepath to "nocard" indicates to the revealCard function 
                // in main that there is no card to display
                temp.imagePath = "nocard";

                return temp;
            }
        }

        //splits the deck in half and returns player1 cards and player2 cards
        public void split(out Card[] player1, out Card[] player2)
        {
            player1 = new Card[26];
            player2 = new Card[26];


            for (int i = 0; cards.Count > 0; i++)
            {
                player1[i] = cards.Dequeue();
                player2[i] = cards.Dequeue();
            }
        }

        //Returns the size of the deck
        public int deckSize()
        {
            return cards.Count;
        }

        //Removes all cards in a deck and returns them
        public Card[] removeAll()
        {
            Card[] temp = cards.ToArray();
            cards.Clear();
            return temp;
        }
        #endregion
    }
}
