# README #

### What is this repository for? ###

This is a simple slap jack card game built using visual objects.  This was built as a group project with 3 other group members in my software engineering class at Weber State University.  The goal was to take an existing card trick game and modify it into a slap jack game while still using as much code as possible from the original card trick game.
